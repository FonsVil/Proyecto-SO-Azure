# Proyecto SO Azure

## Prerequisites

Azure subscription
Python 3.x
    la instalcion de python deberia incluir pip. Puede verificar la version de Python installada cooriendo en el cmd `pip --version`

Crea el ambiente de pruebas

`python -m venv Azure-Face-Detector`

Activa el ambiente de pruebas

`Azure-Face-Detector\Scripts\activate`

Actualizar el pip a su ultima version en el ambiente

    python -m pip install --upgrade pip

Instala la libreria de Azure que se va a utilizar

`pip install --upgrade azure-cognitiveservices-vision-face`

Instalamos mas librerias que vamos a utilizar

`pip install pillow`
`pip install opencv-python`
`pip install torch==1.5.0 torchvision==0.6.0`
`pip install numpy==1.18.4`
`pip install matplotlib==3.2.1`

Este no porque debemos ajustarlo con las versiones pertinentes y hacerle pruebas.
`pip install -r requirements.txt`

Corre el archivo python de deteccion de rostros, emociones y edades.

    python main.py ([NombreArchivo])* 

temporalmente es: `python main.py Prueba1.mp4`
