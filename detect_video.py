from __future__ import division

from io import BufferedReader, BytesIO
from PIL import Image, ImageDraw, ImageFont
from azure.cognitiveservices.vision.face import FaceClient
from msrest.authentication import CognitiveServicesCredentials


from torch.autograd import Variable
import torch
import cv2
import os
import time
# Libreria del grafico
import matplotlib
import matplotlib.pyplot as plt
import numpy as np



# Funciones

def Convertir_RGB(img):
    # Convertir Blue, green, red a Red, green, blue
    b = img[:, :, 0].copy()
    g = img[:, :, 1].copy()
    r = img[:, :, 2].copy()
    img[:, :, 0] = r
    img[:, :, 1] = g
    img[:, :, 2] = b
    return img
def Convertir_BGR(img):
    # Convertir red, blue, green a Blue, green, red
    r = img[:, :, 0].copy()
    g = img[:, :, 1].copy()
    b = img[:, :, 2].copy()
    img[:, :, 0] = b
    img[:, :, 1] = g
    img[:, :, 2] = r
    return img

def Generar_grafico(nombreCarpeta, diccionario):
    # accede al directorio
    os.chdir(nombreCarpeta)
    # Generar un grafico
    fig, ax = plt.subplots()
    # Etiqueta en el eje Y
    ax.set_ylabel('Clases')
    # Etiqueta en el eje X
    ax.set_title('Cantidad de cada deteccion')
    for x in diccionario:
        plt.bar(x, diccionario[x])
    plt.savefig('Grafico.png')
    

'''
- Segun el porcentaje de certeza de cada emocion se devuelve el String con la emocion con mas certeza
@Param azure.cognitiveservices.vision.face.models._models_py3.Emotion
@Return String
'''
def detect_Emotion(emotion):
    # Porcentaje de certeza al que debe superar
    percent_certainty = 95
    # Convertimos emociones en porcentajes
    neutral = emotion.neutral * 100
    happiness = emotion.happiness * 100
    anger = emotion.anger * 100
    sadness = emotion.sadness * 100
    
    contempt = emotion.contempt * 100
    disgust = emotion.disgust * 100
    fear = emotion.fear * 100
    surprise = emotion.surprise * 100
    if neutral > percent_certainty:
        return "Neutral"
    elif happiness > percent_certainty:
        return "Feliz"
    elif anger > percent_certainty:
        return "Enojado"
    elif sadness > percent_certainty:
        return "Triste"
    elif contempt > percent_certainty:
        return "Desprecio"
    elif disgust > percent_certainty:
        return "Disgustado"
    elif fear > percent_certainty:
        return "Temeroso"    
    elif surprise > percent_certainty:
        return "Sorprendido"    
    return "Desconocido"

'''
- Segun el resultado del genero se devuelve un String del genero de la cara detectada y en español
@Param Enum 'Gender'
@Return String
'''
def detect_Gender(gender):
    if gender.value=='male':
        return 'Hombre'
    elif gender.value=='female':
        return 'Mujer'
    return gender.value

def addGrafico(nombreCarpeta, valor, clases):
    if valor in clases:
        clases[valor] += 1
    else:
        clases[valor] = 1
        # crea una nueva carpeta con el nombre de la clase
        os.mkdir(nombreCarpeta)

# Variables para el API AZURE
# This key will serve all examples in this document.
_API_KEY = "488b275b0ed64cf8b34e79fcf6069b3a"
# This endpoint will be used in all examples in this quickstart.
_ENDPOINT = "https://faceapiazureia.cognitiveservices.azure.com"

'''
- Recorre las imagenes del video para aplicarle su debida deteccion, ya sea,
- Edad, Emocion, Genero
- Además, guarda los archivos generados en la carpeta Output
@Param String url_video
@Return void
'''
def deteccion_video(name_video):

    face_client = FaceClient(_ENDPOINT, CognitiveServicesCredentials(_API_KEY))
    
    # Variables locales
    localDir = ""
    start = time.perf_counter()
    clasesEdadDic = {}
    clasesGeneroDic = {}
    clasesEmocionDic = {}


    # Si el computador tiene grafica usarla
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    Tensor = torch.cuda.FloatTensor if torch.cuda.is_available() else torch.FloatTensor
    # Obtiene el video a analizar.
    cap = cv2.VideoCapture("./Files/" + name_video)
    print(str(os.getcwd()))
    # Carpeta donde se guardan los resultados

    os.chdir('Output')  # accede al directorio
    os.mkdir(str(name_video))  # crea una nueva carpeta
    os.chdir(str(name_video))  # accede al directorio
    localDir = str(os.getcwd())

    # Archivo con la informacion
    file = open('Resultados.txt', "w")
    file.write("------------Resultados de la detección------------" + '\n'
                + "Nombre del Video: " + '\n'
                + "Objetos:" + '\n')

    # Crea el archivo avi
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    out = cv2.VideoWriter('output.avi',  fourcc, 5, (1280, 960))

    while cap:
        ret, frame = cap.read()
        # Mostrar Segundos del frame (cap.get(cv2.CAP_PROP_POS_MSEC)/1000)
        if ret is False:
            break
        frame = cv2.resize(frame, (1280, 960),
                            interpolation=cv2.INTER_CUBIC)
        # *******************************************************************
        # PROBLEMA NO HE PODIDO CREAR UN TIPO DE IMAGEN QUE SOPORTE EL AZURE
        # Imagen que vamos a analizar
        RGB_img = Convertir_RGB(frame)
        str_imgarraybytes = RGB_img.tostring()
        img_bytesIO = BytesIO(str_imgarraybytes)
        img_BufferedReader = BufferedReader(img_bytesIO)

        # Usada para obtener una imagen y usarla en el detector de azure 
        # img_file = open(localDir+"\\..\\..\\Files\\foto.jpg",'rb')
        # print(type(img_file))

        with torch.no_grad():
            # Se realiza analisis con el cliente de azure de deteccion de caras.
            response_detection = face_client.face.detect_with_stream(
                image = img_BufferedReader,
                detection_model= 'detection_01',
                recognition_model='recognition_04',
                return_face_attributes=['age','emotion','gender']
            )

        # Abrimos la imagen original para clonarla
        img = Image.open(RGB_img)
        # Usado para dibujar en la imagen
        draw = ImageDraw.Draw(img)
        # Tipo de letra que mostramos resultados en la imagen
        font = ImageFont.truetype('C:\Windows\Fonts\ARLRDBD.ttf',20)

        # Recorremos cada cara detectada
        for face in response_detection:
            age = face.face_attributes.age # Obtenemos la edad
            emotion = detect_Emotion(face.face_attributes.emotion) # Obtenemos las emociones y la convertimos en una unica emocion String
            gender = detect_Gender(face.face_attributes.gender)

            #print("Cara "+ str(face.face_id) +": "+ emotion +" " + str(face.face_attributes.emotion))


            # Obtenemos el rectangulo para pintarlo sobre la cara detectada
            rect = face.face_rectangle
            # Obtenemos la posicion de cada lado del rectangulo que vamos a pintar en la cara detectada
            left = rect.left
            top = rect.top
            right = rect.width + left
            bottom = rect.height + top 
            # Dibujamos el rectangulo verde sobre el cara detectado
            draw.rectangle(((left, top),(right,bottom)), outline = 'green', width=5)

            # Dibujamos letras mostrando la edad de la cara detectada
            draw.text((left, bottom), 'Edad: '+str(int(age)),fill=(255,255,255), font = font, stroke_width=1, stroke_fill="black")
            addGrafico("Edad", str(int(age)), clasesEdadDic)
            # Dibujamos letras mostrando la emocion de la cara detectada
            # Stroke = linea alrededor de letras.
            draw.text((left-45,bottom+20), 'Emocion: '+emotion, fill=(255,255,255), font = font, stroke_width=1, stroke_fill="black",)
            addGrafico("Emocion", emotion, clasesEmocionDic)
            # Dibujamos letras mostrando el genero de la cara detectada
            draw.text((left-45, bottom+40), 'Genero: '+gender,fill=(255,255,255), font = font, stroke_width=1, stroke_fill="black")
            addGrafico("Genero", gender, clasesGeneroDic)

            # Guardamos la nueva imagen detectada
            # img.save("./Output/foto.png")

        # Muestra la nueva imagen modificada con las caras detectadas
        print('Cantidad de personas detectadas: {0}'.format(len(response_detection)))
        img.show()
     # Escribir en el archivo 'Resultados' la cantidad de objetos detectados por clase.
    file.write(
        '\n' + "------------Cantidad de detecciones segun la edad de cada persona detectada ------------" + '\n')
    for x in clasesEdadDic:
        file.write(str(x) + ": " + str(clasesEdadDic[x]) + '\n')
    
    file.write(
        '\n' + "------------Cantidad de detecciones segun la emocion detectada ------------" + '\n')
    for x in clasesEmocionDic:
        file.write(str(x) + ": " + str(clasesEmocionDic[x]) + '\n')  
    
    file.write(
        '\n' + "------------Cantidad de detecciones segun el genero detectada ------------" + '\n')
    for x in clasesGeneroDic:
        file.write(str(x) + ": " + str(clasesGeneroDic[x]) + '\n')
    # Genera los grafico
    Generar_grafico("Edad", clasesEdadDic)
    os.chdir(localDir)
    Generar_grafico("Emocion", clasesEmocionDic)
    os.chdir(localDir)
    Generar_grafico("Genero", clasesGeneroDic)
    os.chdir(localDir)
    # Muestra en consola la duracion total
    print("Nombre del archivo: " + str(name_video) + '\n'
            + f'Tiempo: {time.perf_counter() - start}' + " segundos"
            + '\n' + '\n')

    # Se cierran todos los procesos
    file.close()
    cap.release()
    cv2.destroyAllWindows()
