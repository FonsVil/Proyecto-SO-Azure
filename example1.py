import asyncio
import io
import json
import glob
import os
import sys
import time
import uuid
from msrest.pipeline import Response
import requests
from urllib.parse import urlparse
from io import BytesIO
# To install this module, run:
# python -m pip install Pillow
from PIL import Image, ImageDraw
from azure.cognitiveservices.vision.face import FaceClient
from msrest.authentication import CognitiveServicesCredentials
from azure.cognitiveservices.vision.face.models import TrainingStatusType, Person


# This key will serve all examples in this document.
API_KEY = "488b275b0ed64cf8b34e79fcf6069b3a"

# This endpoint will be used in all examples in this quickstart.
ENDPOINT = "https://faceapiazureia.cognitiveservices.azure.com"

#credential = json.load(open('AzureCloudKeys.json'))
#API_KEY = credential['API_KEY']
#ENDPOINT = credential['ENDPOINT']

face_client = FaceClient(ENDPOINT, CognitiveServicesCredentials(API_KEY))


# Detect a face in an image that contains a single face
single_face_image_url = 'https://www.biography.com/.image/t_share/MTQ1MzAyNzYzOTgxNTE0NTEz/john-f-kennedy---mini-biography.jpg'
single_image_name = os.path.basename(single_face_image_url)
# We use detection model 3 to get better performance.
detected_faces = face_client.face.detect_with_url(url=single_face_image_url, detection_model='detection_03')
if not detected_faces:
    raise Exception('No face detected from image {}'.format(single_image_name))

# Display the detected face ID in the first single-face image.
# Face IDs are used for comparison to faces (their IDs) detected in other images.
print('Detected face ID from', single_image_name, ':')
for face in detected_faces: print (face.face_id)
print()

# Save this ID for use in Find Similar
first_image_face_ID = detected_faces[0].face_id
#/////////////////////////////////////////////////////////////////////////////////////////////


#   image_url = 'https://www.semana.com/resizer/_yZcCMUbrHjA4c2lKHKsMlNSMFA=/1200x675/filters:format(jpg):quality(50)//cloudfront-us-east-1.images.arcpublishing.com/semana/2GJU5TUOQNBCZMCKVSZZ6UFBQA.jpg'
image_url = 'https://www.danceacademyusa.com/wp-content/uploads/2020/04/new1.jpg'
image_name = os.path.basename(image_url)
response_detected_faces = face_client.face.detect_with_url(
    image_url,
    detection_model='detection_03',
    recognition_model='recognition_04'
)
print(response_detected_faces)
if not response_detected_faces:
    raise Exception('No face detected')
print('Number of people detected: {0}'.format(len(response_detected_faces)))

