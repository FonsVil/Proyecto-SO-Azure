import sys
import time
import concurrent.futures

# importamos la funcion que analiza cada video
from exmple3 import deteccion_video

if __name__ == "__main__":
    listaURLVideos = []
    # Agregamos las direcciones del video que desea analizar.
    for i in range(1,len(sys.argv)):
        listaURLVideos.append(str(sys.argv[i]))
    print(listaURLVideos)
    '''
    El método ThreadPoolExecutor trabaja de manera similar a ProcessPoolExecutor, pero se trata de mecanismos distintos:
    ProcessPoolExecutor ejecuta cada worker como un proceso hijo separado   
    ThreadPoolExecutor ejecuta cada uno de los workers como un subproceso (thread) dentro del proceso principal
    '''
    # El método "ProcessPoolExecutor" permite 
    # ejecutar varias instancias (workers) 

    print("-----------------Datos de ejecucion-----------------" + '\n')
    with concurrent.futures.ThreadPoolExecutor() as executor:
        for url_archivo in listaURLVideos:
            #executor.submit(funcion,listaURLVideos[0],1)
            #executor.submit(funcion,listaURLVideos[1],0)
            executor.submit(deteccion_video(url_archivo))
    print("----------------------------------------------------")
    

    
